﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.costIn = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.applianceBox = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.neededPower = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.hourUse = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.calcButton = New System.Windows.Forms.Button()
        Me.ansLabel = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.gallonUse = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.gallonCost = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'costIn
        '
        Me.costIn.Location = New System.Drawing.Point(172, 12)
        Me.costIn.Name = "costIn"
        Me.costIn.Size = New System.Drawing.Size(100, 20)
        Me.costIn.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Enter cost per kwh"
        '
        'applianceBox
        '
        Me.applianceBox.FormattingEnabled = True
        Me.applianceBox.Items.AddRange(New Object() {"Fridge", "Fan", "TV", "Space Heater", "Dryer", "Oven", "Laundry Washer"})
        Me.applianceBox.Location = New System.Drawing.Point(151, 38)
        Me.applianceBox.Name = "applianceBox"
        Me.applianceBox.Size = New System.Drawing.Size(121, 21)
        Me.applianceBox.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(87, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Select Appliance"
        '
        'neededPower
        '
        Me.neededPower.Location = New System.Drawing.Point(171, 66)
        Me.neededPower.Name = "neededPower"
        Me.neededPower.Size = New System.Drawing.Size(100, 20)
        Me.neededPower.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(102, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Enter Watts needed"
        '
        'hourUse
        '
        Me.hourUse.Location = New System.Drawing.Point(170, 93)
        Me.hourUse.Name = "hourUse"
        Me.hourUse.Size = New System.Drawing.Size(100, 20)
        Me.hourUse.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 100)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(99, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Enter hours per day"
        '
        'calcButton
        '
        Me.calcButton.Location = New System.Drawing.Point(96, 174)
        Me.calcButton.Name = "calcButton"
        Me.calcButton.Size = New System.Drawing.Size(75, 23)
        Me.calcButton.TabIndex = 8
        Me.calcButton.Text = "Calculate"
        Me.calcButton.UseVisualStyleBackColor = True
        '
        'ansLabel
        '
        Me.ansLabel.AutoSize = True
        Me.ansLabel.Location = New System.Drawing.Point(16, 174)
        Me.ansLabel.Name = "ansLabel"
        Me.ansLabel.Size = New System.Drawing.Size(0, 13)
        Me.ansLabel.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 126)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(94, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Enter gallons used"
        Me.Label5.Visible = False
        '
        'gallonUse
        '
        Me.gallonUse.Location = New System.Drawing.Point(170, 123)
        Me.gallonUse.Name = "gallonUse"
        Me.gallonUse.Size = New System.Drawing.Size(100, 20)
        Me.gallonUse.TabIndex = 11
        Me.gallonUse.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 149)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(98, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Enter cost of gallon"
        Me.Label6.Visible = False
        '
        'gallonCost
        '
        Me.gallonCost.Location = New System.Drawing.Point(170, 148)
        Me.gallonCost.Name = "gallonCost"
        Me.gallonCost.Size = New System.Drawing.Size(100, 20)
        Me.gallonCost.TabIndex = 13
        Me.gallonCost.Visible = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.gallonCost)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.gallonUse)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.ansLabel)
        Me.Controls.Add(Me.calcButton)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.hourUse)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.neededPower)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.applianceBox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.costIn)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents costIn As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents applianceBox As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents neededPower As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents hourUse As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents calcButton As Button
    Friend WithEvents ansLabel As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents gallonUse As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents gallonCost As TextBox
End Class
