﻿Imports System.Data
Imports System.Data.SqlClient

Public Class Form1

    Dim sqlCommand As New SqlCommand
    Dim sqlCon As New SqlConnection(conString)
    Dim conString As String = "Data Source=.\SQLEXPRESS;Database=CostCalculator;Integrated Security=True"
    Dim dataIn As String
    Dim costInput As Double
    Dim useRangeMin As Double
    Dim useRangeMax As Double
    Dim powerInput As Double
    Dim hourInput As Double
    Dim totalCost As Double
    Dim gallonInput As Double
    Dim gallonPrice As Double
    Dim galRangeMin As Double
    Dim galRangeMax As Double

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles costIn.TextChanged
        If costIn.Text = "" Or Not IsNumeric(costIn.Text) Then
            MessageBox.Show("Please enter a valid number for the cost.")
        Else costInput = Convert.ToDouble(costIn.Text)
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles applianceBox.SelectedIndexChanged
        If applianceBox.SelectedItem.ToString() = "Fridge" Then
            Label5.Visible = False
            Label6.Visible = False
            gallonUse.Visible = False
            gallonCost.Visible = False
            useRangeMin = 50.0
            useRangeMax = 58.79
        ElseIf applianceBox.SelectedItem.ToString() = "Fan"
            Label5.Visible = False
            Label6.Visible = False
            gallonUse.Visible = False
            gallonCost.Visible = False
            useRangeMin = 88.0
            useRangeMax = 370.0
        ElseIf applianceBox.SelectedItem.ToString() = "TV"
            Label5.Visible = False
            Label6.Visible = False
            gallonUse.Visible = False
            gallonCost.Visible = False
            useRangeMin = 23.0
            useRangeMax = 286.0
        ElseIf applianceBox.SelectedItem.ToString() = "Space Heater"
            Label5.Visible = False
            Label6.Visible = False
            gallonUse.Visible = False
            gallonCost.Visible = False
            useRangeMin = 1000.0
            useRangeMax = 1500.0
        ElseIf applianceBox.SelectedItem.ToString() = "Dryer"
            Label5.Visible = False
            Label6.Visible = False
            gallonUse.Visible = False
            gallonCost.Visible = False
            useRangeMin = 300.0
            useRangeMax = 400.0
        ElseIf applianceBox.SelectedItem.ToString() = "Oven"
            Label5.Visible = False
            Label6.Visible = False
            gallonUse.Visible = False
            gallonCost.Visible = False
            useRangeMin = 4000.0
            useRangeMax = 8000.0
        ElseIf applianceBox.SelectedItem.ToString() = "Laundry Washer"
            Label5.Visible = True
            Label6.Visible = True
            gallonUse.Visible = True
            gallonCost.Visible = True
            useRangeMin = 400
            useRangeMax = 1300
            galRangeMin = 15
            galRangeMax = 30
        End If
    End Sub

    Private Sub neededPower_TextChanged(sender As Object, e As EventArgs) Handles neededPower.TextChanged
        If neededPower.Text = "" Or Not IsNumeric(neededPower.Text) Then
            MessageBox.Show("Please enter a valid number for the power.")
        Else powerInput = Convert.ToDouble(neededPower.Text)
        End If
    End Sub

    Private Sub hourUse_TextChanged(sender As Object, e As EventArgs) Handles hourUse.TextChanged
        If hourUse.Text = "" Or Not IsNumeric(hourUse.Text) Then
            MessageBox.Show("Please enter a valid number for the hours used.")
        Else hourInput = Convert.ToDouble(hourUse.Text)
        End If
    End Sub

    Private Sub gallonUse_TextChanged(sender As Object, e As EventArgs) Handles gallonUse.TextChanged
        If gallonUse.Text = "" Or Not IsNumeric(gallonUse.Text) Then
            MessageBox.Show("Please enter a valid number for the gallons used.")
        Else gallonInput = Convert.ToDouble(gallonUse.Text)
        End If
    End Sub

    Private Sub gallonCost_TextChanged(sender As Object, e As EventArgs) Handles gallonCost.TextChanged
        If gallonCost.Text = "" Or Not IsNumeric(gallonCost.Text) Then
            MessageBox.Show("Please enter a valid cost for the gallons used.")
        Else gallonPrice = Convert.ToDouble(gallonCost.Text)
        End If
    End Sub

    Private Sub calcButton_Click(sender As Object, e As EventArgs) Handles calcButton.Click
        If costIn.Text = "" Or Not IsNumeric(costIn.Text) Then
            MessageBox.Show("Please enter a number for cost.")
        ElseIf neededPower.Text = "" Or Not IsNumeric(neededPower.Text)
            MessageBox.Show("Please enter a number for power.")
        ElseIf hourUse.Text = "" Or Not IsNumeric(hourUse.Text)
            MessageBox.Show("Please enter a number for hour use.")
        End If
        If powerInput > useRangeMax Or powerInput < useRangeMin Then
            MessageBox.Show("That is not a valid power range for this appliance. Between " + useRangeMin.ToString + " and " + useRangeMax.ToString + ".")
        End If
        If applianceBox.SelectedItem() = "Laundry Washer" Then
            totalCost = (((powerInput * hourInput) / 1000) * costInput) + (gallonInput * gallonPrice)
            MessageBox.Show("Your total cost with the input given is $" + totalCost.ToString)
        Else
            totalCost = ((powerInput * hourInput) / 1000) * costInput
            MessageBox.Show("Your total cost with the input given is $" + totalCost.ToString)
        End If

        sqlCon.ConnectionString = conString
        sqlCon.Open()
        sqlCommand.Connection = sqlCon
        sqlCommand.CommandText = "INSERT INTO [Table] (appType, timeUse, allCost) VALUES (@appType, @timeUse, @allCost)"
        sqlCommand.Parameters.AddWithValue("@appType", applianceBox.SelectedItem)
        sqlCommand.Parameters.AddWithValue("@timeUse", hourInput)
        sqlCommand.Parameters.AddWithValue("@allCost", totalCost)
        sqlCommand.ExecuteNonQuery()
        sqlCon.Close()

        Form2.Show()
    End Sub
End Class
